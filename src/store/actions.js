export const INCREMENT = 'INCREMENT';
export const DECREMENT = 'DECREMENT';


export const test = () => {
  return {
    type: INCREMENT
  }
}
export const increment = () => {
  return dispatch => {
    setTimeout(() => {
        dispatch((
          // HTTP call here
          () => {
            return {
              type: INCREMENT
            }
          }
        )());
      }, 2000);
  }
}

export const decrement = () => {
  return dispatch => {
    setTimeout(() => {
        dispatch((
          // HTTP call here
          () => {
            return {
              type: DECREMENT
            }
          }
        )());
      }, 2000);
  }
}