import { combineReducers } from 'redux'
import CounterReducer from './reducer/counterReducer'

const reducerList = combineReducers({
  counter: CounterReducer
});

export default reducerList;