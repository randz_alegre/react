import React, { Component } from 'react';
import CounterOutput from '../../component/CounterOutput/CounterOutput';
import { connect } from 'react-redux'
import * as actionType from '../../store/actions'

class Counter extends Component{
  render(){
    return(
      <div>
        {this.props.counter.counter}
        <CounterOutput increment={this.props.onIcrementCounter} decrement={this.props.onDecrementCounter}></CounterOutput>
      </div>  
    )
  };
}

const mapStateToProps = (state) => {
  return {
    ...state
  }
}

const mapDispatchToProps = (dispatch) => {
 return {
   onIcrementCounter: () => dispatch(actionType.increment()),
   onDecrementCounter: () => dispatch(actionType.decrement()),
 }
}

export default connect(mapStateToProps, mapDispatchToProps)(Counter);