import React from 'react'
import CounterControl from '../CounterControl/CounterControl'

const counterOuput = (props) => {
  return(
    <div>
      <CounterControl increment={props.increment} decrement={props.decrement}></CounterControl>
    </div>
  );
}

export default counterOuput;